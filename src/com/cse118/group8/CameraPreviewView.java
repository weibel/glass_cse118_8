package com.cse118.group8;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.view.View;

//Create a processed preview Object so that the camera can see the changes in real time 
public class CameraPreviewView extends View implements Camera.PreviewCallback {
	//flag for keeping track if it's processing the image or not
	private boolean processing;
	private byte[] mData;
	private int[] rgb;
	private Bitmap bmp;

	//chose which view to display
	private int viewColor;
	
	//PreviewImageSize - trying to get this work with multiple resolution, worse resolution = faster...
	private Size previewImageSize;
	
	//paint object to show which one is currently being displayed
	private Paint paint;
	
	//Constructor
	public CameraPreviewView(Context context) {
		super(context);
		paint = new Paint();

	}

	//reset method initalize everything to 0 
	public void reset(Size previewImageSize) {
		this.previewImageSize = previewImageSize;
		processing = false;
		mData = new byte[0];
		rgb = new int[0];
	}
	
	@Override
	public void onPreviewFrame(byte[] inputData, Camera camera) {
		// TODO Auto-generated method stub
		if (processing == true) {
			return;
		}
		processing = true;
		
		if (mData.length != inputData.length) {
			mData = new byte[inputData.length];
		}
		System.arraycopy(inputData, 0, mData, 0, inputData.length);
		
		//force ondrawdraw
		invalidate();
	}
	
	public void onDraw(Canvas canvas) {
		if (mData.length == 0) {
			return;
		}
		
		int width = previewImageSize.width;
		int height = previewImageSize.height;
		
		if (rgb.length != width*height) {
			rgb = new int[width*height];
		}

		decodeYUV444SP(rgb, mData, width, height);
		bmp = Bitmap.createBitmap(rgb, width, height,Bitmap.Config.RGB_565);
		canvas.drawBitmap(bmp, 0, 0, null);
		
		if (getViewColor() == 0) {
			canvas.drawText("Protanopia - Magenta + Green", 50, 50, paint);
		} else if (getViewColor() == 1) {
			canvas.drawText("Deuteranopia - Red + Cyan", 50, 50, paint);
		} else {
			canvas.drawText("No Filter", 50, 50, paint);
		}
		
		//change flag to be ready state
		processing = false;
	}
	
	//Getter method for View Color, options are 0,1,2
	public int getViewColor() {
		return viewColor;
	}
	
	public void setViewColor(int i) {
		viewColor = i;
	}
	
	
	//method copied from camera app with some changes to change the colors
	//https://android.googlesource.com/platform/packages/apps/Camera.git/+/android-4.1.2_r2/jni/feature_mos_jni.cpp
	public void decodeYUV444SP(int[] rgb, byte[] yuv420sp, int width, int height) {
		final int frameSize = width * height;
		for (int j = 0, yp = 0; j < height; j++) {
			int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
			for (int i = 0; i < width; i++, yp++) {
				int y = (0xff & ((int) yuv420sp[yp])) - 16;
				if (y < 0) y = 0;
				if ((i & 1) == 0) {
					v = (0xff & yuv420sp[uvp++]) - 128;
					u = (0xff & yuv420sp[uvp++]) - 128;
				}

				int y1192 = 1192 * y;
				int r = (y1192 + 1634 * v);
				int g = (y1192 - 833 * v - 400 * u);
				//no blue for now. it's initialized only for third case
				int b = (y1192 + 2066 * u);

				if (r < 0) r = 0; else if (r > 262143) r = 262143;
				if (g < 0) g = 0; else if (g > 262143) g = 262143;
				///Initialized only for third case
				if (b < 0) b = 0; else if (b > 262143) b = 262143;
				if (getViewColor() == 0) {
					//merges red/blue to make magenta for protanopia
					//http://www.color-blindness.com/2006/11/16/protanopia-red-green-color-blindness/
					rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((r >> 10) & 0xff);
				} else if (getViewColor() == 1) {
					//merges blue/green to make cyan for deuteranopia
					//http://www.color-blindness.com/2007/04/17/deuteranopia-red-green-color-blindness/
					rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((g >> 10) & 0xff);
				} else {
					//default
					rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
				}
			}
		}
	}

}

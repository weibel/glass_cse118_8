package com.cse118.group8;

import java.io.File;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera.Size;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;
import android.widget.FrameLayout;

/*
 * CLASS MainActivity
 */
public class MainActivity extends Activity {

	private GestureDetector gestureDetector;
	private ImageView imageView;
	private TextView textView;
	private CameraPreviewView cameraPreviewView;
	private CameraSurfaceView cameraSurfaceView;
	private FrameLayout frame;
	
	private SharedPreferences sharedPreference;
	private SharedPreferences.Editor sharedPreferenceEditor;
	
	
	@Override
	/*
	 * Direct tap events to gestureDetector
	 */
	public boolean onGenericMotionEvent(MotionEvent event)
	{
		gestureDetector.onTouchEvent(event);
		return true;
	}

	@Override
	/*
	 * Standard function for every app
	 * 
	 * Call this when the app starts
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//make sure the window is brightest it can be... Don't know how important this is.
		WindowManager.LayoutParams layout = getWindow().getAttributes();
		layout.screenBrightness = 1F;
		getWindow().setAttributes(layout);
		
		//Create the objects 
		gestureDetector = new GestureDetector(this, new MyGestureListener());
		cameraPreviewView = new CameraPreviewView(this);
		cameraSurfaceView = new CameraSurfaceView(this, cameraPreviewView);
		
		SharedPreferences preferences = getPreferences(MODE_PRIVATE);
		int width = preferences.getInt("previewWidth", 0);
		int height = preferences.getInt("previewHeight", 0);
		cameraSurfaceView.setPreviewSize(width, height);
		cameraPreviewView.setViewColor(0);
		//TODO add extra color changes
		setContentView(R.layout.activity_main);
		frame = (FrameLayout) findViewById(R.id.frame);
		frame.addView(cameraSurfaceView);
		frame.addView(cameraPreviewView);
		
	}
	
	protected void onDestroy()
	{
		super.onDestroy();
		sharedPreferenceEditor = sharedPreference.edit();
		sharedPreferenceEditor.putInt(Settings.COLOR_PREF_STRING, -1);
		sharedPreferenceEditor.commit();
	}


	/*
	 * GestureListener for glass to respond to tap
	 */
	class MyGestureListener extends android.view.GestureDetector.SimpleOnGestureListener
	{
		@Override
		public boolean onDoubleTap(MotionEvent e) {
			int temp = cameraPreviewView.getViewColor();
			temp = ((temp + 1) % 3);
			cameraPreviewView.setViewColor(temp);
			return true;
		}
	}
}

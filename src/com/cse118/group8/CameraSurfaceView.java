package com.cse118.group8;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceView;
import android.view.SurfaceHolder;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Size;


public class CameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback
{
        private SurfaceHolder holder;
        private Camera camera;
        private CameraPreviewView CameraPreviewView;
        private List<Size> supportedPreviewSizes;
        private int previewWidth = 0;
        private int previewHeight = 0;
 
        
        public CameraSurfaceView(Context context) {
            super(context);
            init();
        }

        public CameraSurfaceView(Context context, AttributeSet attrs) {
            this(context, attrs,0);
            init();
        }

        public CameraSurfaceView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }
     
        public CameraSurfaceView(Context context, CameraPreviewView cameraPreviewView) {
        	super(context);
        	init();
        	CameraPreviewView = cameraPreviewView;
        }
        
        private void init()
        {
        	//Initiate the Surface Holder properly
            this.holder = this.getHolder();
            this.holder.addCallback(this);
            this.holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
        
        @Override
        public void surfaceCreated(SurfaceHolder holder) 
        {
        		//open camera here
        		camera = Camera.open();
                try
                {
                       Camera.Parameters parameters = camera.getParameters();
                       //grab the supported resolutions
                       supportedPreviewSizes = parameters.getSupportedPreviewSizes();
                       camera.setPreviewDisplay(holder);
                }
                catch(IOException ioe)
                {
                		//flushout the camera object
                		camera.release();
                		camera = null;
                        ioe.printStackTrace(System.out);
                }
        }

        //getter method for supported preview sizes
        public List<Size> getSupportedPreviewSizes() {
        	return supportedPreviewSizes;
        }
        
        //setter methods to set preview sizes
        //set using a size ojbect
        public void setPreviewSize(Size previewSize) {
        	setPreviewSize(previewSize.width, previewSize.height);
        }
        //actual setter to set the preview sizes
        public void setPreviewSize(int width, int height) {
        	previewWidth = width;
        	previewHeight = height;
        }
        
        //getter method to get width and height
        public int getPreviewWidth() {
        	return previewWidth;
        }
        
        public int getPreviewHeight() {
        	return previewHeight;
        }
        
        
        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) 
        {
        	camera.setPreviewCallback(null);
        	camera.stopPreview();
        	
        	//Setup the default preview size TODO will try to add more sizes
        	Camera.Parameters parameters = camera.getParameters();
        	
        	if (previewWidth == 0 || previewHeight == 0) {
        		setPreviewSize(parameters.getPreviewSize());
        	}
        	
            setPreviewSize(parameters.getPreviewSize());

            // Set preview size for the camera object
            parameters.setPreviewSize(previewWidth, previewHeight);
            
            
            //restart the previewview with new size set the call back and start the preview
            CameraPreviewView.reset(parameters.getPreviewSize());
            camera.setPreviewCallback(CameraPreviewView);
            Camera.Parameters params = camera.getParameters();
            params.setPreviewFpsRange(30000, 30000);
            camera.setParameters(params);
            camera.startPreview();
        }


        @Override
        public void surfaceDestroyed(SurfaceHolder holder) 
        {
                // Surface will be destroyed when replaced with a new screen
                //Always make sure to release the Camera instance
                camera.setPreviewCallback(null);
        		camera.stopPreview();
                camera.release();
                camera = null;
        }
        
        public Camera getCamera()
        {
                return this.camera;
        }
}
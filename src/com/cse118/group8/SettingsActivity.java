package com.cse118.group8;

import java.util.ArrayList;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class SettingsActivity extends Activity implements OnItemClickListener {
	private SharedPreferences sharedPreference;
	private SharedPreferences.Editor sharedPreferenceEditor;
	private int colorPreference;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		 ArrayList<String> options = new ArrayList<String>();
         options.add("Red");
         options.add("Green");
         options.add("Both");
         options.add("Continuous");
         // This is the array adapter, it takes the context of the activity as a first // parameter, the type of list view as a second parameter and your array as a third parameter
         ArrayAdapter<String> arrayAdapter =      
         new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, options);
         
		final ListView listView = new ListView(this);
		listView.setAdapter(arrayAdapter);
        listView.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            	listView.smoothScrollToPositionFromTop(position, 0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        
        listView.setOnItemClickListener(this);
        setContentView(listView);
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();

		sharedPreference = getSharedPreferences(Settings.SHARED_PREF_KEY, MODE_PRIVATE);
		colorPreference = sharedPreference.getInt(Settings.COLOR_PREF_STRING, -1);
		
		if (colorPreference != -1)
			this.finish();
	}

	private void setPreference(int i)
	{
		if (i != -1)
		{
			sharedPreferenceEditor = sharedPreference.edit();
			sharedPreferenceEditor.putInt(Settings.COLOR_PREF_STRING, i);
			sharedPreferenceEditor.commit();
		}
		this.finish();
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
		this.setPreference(position);
	}
}